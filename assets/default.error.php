<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?= config('TITLE_PREFIX') ?>Error: <?= $errorMessage ?></title>
</head>

<body>

<div id="content">

	<h1><?= $errorMessage ?></h1>

	<h2>Error information</h2>
	<p><strong>Date:</strong> <?= $errorDate ?></p>
	<p><strong>Script:</strong> <?= $errorScript ?></p>
	<p><strong>Line:</strong> <?= $errorLine ?></p>


	<?php if(isErrorDebugging()) { ?>

		<h2>Runtime stack trace</h2>
		<p>
			<?php
			$evodd = 0;

			foreach ($stackTrace as $n => $i) {

				$runtimeStack = "";

				$i['file'] = relative_path($i['file']);

				$argList = array();
				if (is_array($i['args'])) {
					foreach ($i['args'] as $argNum => $arg) {
						if (is_string($arg) && strlen($arg) > 128) {
							array_push($argList, "<span style=\"color: #DD0000\">\"{$arg}\"</span>");
						} else if (is_object($arg)) {
							array_push($argList, "<span style=\"color: #DD0000\">\"[object:" . get_class($arg) . "]\"</span>");
						} else if (is_resource($arg)) {
							array_push($argList, "<span style=\"color: #DD0000\">\"[resource:" . print_r($arg, true) . "]\"</span>");
						} else if (is_array($arg)) {
							array_push($argList, "<span style=\"color: #DD0000\">\"[array:" . sizeof($arg) . "]\"</span>");
						} else {
							array_push($argList, "<span style=\"color: #DD0000\">\"{$arg}\"</span>");
						}
					}

					$argList = join("<span style=\"color: #007700\">,</span> ", $argList);
				} else {
					$argList = "";
				}

				$runtimeStack .= "\r\n<br><b> at <span style=\"color: #007700\">[{$n}]</span> {$i['file']}<span style=\"color: #FF8000\">:{$i['line']}</span></b>";
				$runtimeStack .= "\r\n<br><b>&nbsp;&nbsp;&raquo; <span style=\"color: #0000BB\">{$i['function']}</span><span style=\"color: #007700\">(</span>{$argList}<span style=\"color: #007700;\">)</span><span style=\"color: #0000BB\">;</span></b>";

				echo $runtimeStack;
			}
			?>
			<br /><br />
		</p>

		<h2>Log messages</h2>
		<p><?= ($runtimeLog) ? join('<br />', $runtimeLog) : "No messages were recorded in the log"; ?></p>

		<h2>Request parameters</h2>
		<p>
			<?php
			echo "<table width=\"500\" cellpadding=\"3\" cellspacing=\"0\">";

			foreach($_POST as $key => $value) {
				$evodd++; $evc = ($evodd%2==0) ? "#EEEEEE" : "#DDDDDD";
				echo "<tr bgcolor=\"{$evc}\">
									<td><span style=\"color: #007700\">POST</span></td>
									<td><span style=\"color: #0000BB\">\"{$key}\"</span></td>
									<td><span style=\"color: #DD0000\">\"".print_r($value, true)."\"</span></td>
								</tr>";
			}

			foreach($_GET as $key => $value) {
				$evodd++; $evc = ($evodd%2==0) ? "#EEEEEE" : "#DDDDDD";
				echo "<tr bgcolor=\"{$evc}\">
									<td><span style=\"color: #007700\">GET</span></td>
									<td><span style=\"color: #0000BB\">\"{$key}\"</span></td>
									<td><span style=\"color: #DD0000\">\"".print_r($value, true)."\"</span></td>
								</tr>";
			}

			echo "</table>";
			?>
		</p>

		<h2>Session variables</h2>
		<p>
			<?php
			echo "<table width=\"500\" cellpadding=\"3\" cellspacing=\"0\">";
			foreach((array) $_SESSION as $key => $value) {
				$evodd++; $evc = ($evodd%2==0) ? "#EEEEEE" : "#DDDDDD";
				echo "<tr bgcolor=\"{$evc}\">
									<td><span style=\"color: #0000BB\">\"{$key}\"</span></td>
									<td><span style=\"color: #DD0000\">\"".print_r($value, true)."\"</span></td>
								</tr>";
			}
			echo "</table>";
			?>
		</p>

		<h2>Cookies</h2>
		<p>
			<?php
			echo "<table width=\"500\" cellpadding=\"3\" cellspacing=\"0\">";
			foreach($_COOKIE as $key => $value) {
				$evodd++; $evc = ($evodd%2==0) ? "#EEEEEE" : "#DDDDDD";
				echo "<tr bgcolor=\"{$evc}\">
									<td><span style=\"color: #0000BB\">\"{$key}\"</span></td>
									<td><span style=\"color: #DD0000\">\"".print_r($value, true)."\"</span></td>
								</tr>";
			}
			echo "</table>";
			?>
		</p>

		<h2>Exception</h2>
		<p>
			<pre>
				<?php var_dump($e); ?>
			</pre>
		</p>

		<h2>Loaded modules</h2>
		<p>
			<?php
			if(!class_exists("Module")) {
				$loadedModules = "Module managemenent class was not loaded";
			} else {
				$loadedModules = "<table width=\"500\" cellpadding=\"3\" cellspacing=\"0\">";
				foreach(ModuleManager::$modules as $module) {
					$evodd++; $evc = ($evodd%2==0) ? "#EEEEEE" : "#DDDDDD";
					$loadedModules .= "
								<tr bgcolor=\"{$evc}\">
									<td><span style=\"color: #0000BB\"><b>{$module['name']}</b>, by {$module['author']}</span></td><td><span style=\"color: #DD0000\">{$module['version']}</span></td><td><span style=\"color: #007700\">{$module['base']}</span></td>
								</tr>";
				}
				$loadedModules .= "</table>";
			}

			echo $loadedModules;
			?>
		</p>

		<h2>Output buffer</h2>
		<textarea class="output-buffer"><?= ($outputBuffer) ?></textarea>

	<?php } ?>

	<h5>Powered by Diesel Framework 3.1 - LQDI Technologies - 2013</h5>

</div>

<style>body{margin:0;padding:0;font-family:Helvetica,Arial,sans-serif;font-size:14px;color:#333;background-color:#fff}#content{margin:20px 40px 0 40px;padding:0}#content p{margin:12px 20px 12px 0}#content h2{background-color:transparent;border-bottom:1px solid #999;color:#000;font-size:18px;font-weight:bold;margin:28px 0 16px 0;padding:5px 0 6px 0}#content h1{font-size: 18px; font-weight: bold; background:#fbe6f2;border:1px solid #d893a1;color:#333;margin:10px 0 5px 0;padding:10px}.output-buffer{width: 100%;height: 100%;font-size: 10px;line-height: 12px;font-family: monospace;}</style>

</body>


</html>