<?php
if(!defined('CLI_MODE')) {
	session_start();
}

Database::$logQueries = false; // Disable query logging briefly so we avoid logging a lot of boilerplate

if(config('ERROR_DEBUGGING')) {
	ini_set('xdebug.var_display_max_children', '4096');
	ini_set('xdebug.var_display_max_data', '65535');
	ini_set('xdebug.var_display_max_depth', '32');
}

$connID = Database::quickConnect(config('DEFAULT_DATABASE'));
ActiveModelManager::setup($connID);

Session::init("User");
Session::reload();

Database::$logQueries = true; // Re-enable log querying for debugging
