<?php
/**
 * Diesel Framework
 * Copyright © LQDI Technologies - 2011
 * http://www.lqdi.net
 *
 * Application entry-point
 * Entry-point da aplicação
 *
 * @author Aryel Tupinambá
 */

if(php_sapi_name() == "cli") {

    define('DF3_PATH', str_replace("\\","/",dirname(__FILE__))."/df3/");
    define('CLI_MODE', true);

} else {
    if(!defined('DF3_PATH')) {
        define('DF3_PATH', './df3/');
    }
}

if(file_exists(DF3_PATH . "../environment.conf.php")) {
	include(DF3_PATH . "../environment.conf.php");
}

if(!defined('ENVIRONMENT')) {
	define('ENVIRONMENT', 'development');
}

include(DF3_PATH . "init.php");

Diesel::useFixedNamingScheme();

// Gets the requested route from the URL or CLI parameters
$route = Diesel::getRequestedRoute();

// Parses the route into a framework path
$path = Router::parseRoute($route);

// Ensures all required information is in the path
if(!$path['module']) {
    $path['module'] = "app";
}

if(!$path['controller']) {
    $path['controller'] = config("DEFAULT_CONTROLLER");
}

if(!$path['method']) {
    $path['method'] = "index";
}

if(!$path['parameters']) {
    $path['parameters'] = array();
}

Diesel::$context = $path['module'];

define("DIESEL_CONTEXT_SET", true);

// Runs the application init script (pre-execute event)
include(APP_PATH . "app.init.php");

// Executes the incoming app route
Diesel::execute($path);

// Unload all to free memory controllers
Diesel::cleanup();
