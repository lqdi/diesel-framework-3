<?php
/**
 * Diesel Framework
 * Copyright © LQDI Technologies - 2013
 * http://www.lqdi.net
 *
 * @author Aryel Tupinambá
 */

require_once(DF3_PATH . "lib/JSMin.php");
require_once(DF3_PATH . "sass/SassParser.php");

class Assets {

	private static $cacheFlag = false;
	private static $cacheExternalFlag = false;
	private static $compressFlag = false;
	private static $forceCacheSkip = true;

	private static $numFiles = array('js' => 0, 'css' => 0);
	private static $cacheHashCSS = null;
	private static $cacheHashJS = null;

	private static $computedMtime = array('js' => 0, 'css' => 0);

	private static $_vendorLibs = array(
		'jquery' => '//cdnjs.cloudflare.com/ajax/libs/jquery/1.12.3/jquery.min.js',
		'jqueryui' => '//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js',
	);

	private static $_dieselLibs = array(
		'df3' => 'df3/js/df3.js'
	);

	private static $assetList = array(
		'version' => null,
		'css' => array(),
		'scss' => array(),
		'js' => array(),
		'vendor' => array(),
		'df3' => array()
	);

	public static function version($version) {
		self::$assetList['version'] = $version;
	}

	public static function setCachingFlag($flag) {
		self::$cacheFlag = $flag;
	}

	public static function setExternalCachingFlag($flag) {
		self::$cacheExternalFlag = $flag;
	}

	public static function setCompressionFlag($flag) {
		self::$compressFlag = $flag;
	}

	public static function setForceCacheSkip($flag) {
		self::$forceCacheSkip = $flag;
	}

	public static function css() {
		$list = func_get_args();
		self::computeMtimes('css', $list);
		self::$assetList['css'] = array_merge(self::$assetList['css'], $list);
	}

	public static function scss() {
		$list = func_get_args();
		self::computeMtimes('css', $list);
		self::$assetList['scss'] = array_merge(self::$assetList['scss'], $list);
	}

	public static function javascript() {
		$list = func_get_args();
		self::computeMtimes('js', $list);
		self::$assetList['js'] = array_merge(self::$assetList['js'], $list);
	}

	public static function vendorLibs() {
		$list = func_get_args();
		self::$assetList['vendor'] = array_merge(self::$assetList['vendor'], $list);
	}

	public static function dieselLibs() {
		$list = func_get_args();
		self::$assetList['df3'] = array_merge(self::$assetList['df3'], $list);
	}

	private static function computeMtimes($type, $list) {
		foreach($list as $file) {
			$path = APP_PATH . "../assets/" . $file;
			if(!file_exists($path)) { continue; }
			self::$numFiles[$type]++;
			self::$computedMtime[$type] += filemtime($path);
		}
	}

	private static function isExternal($path) {
		return (substr($path,0,7) == "http://" || substr($path,0,8) == "https://" || substr($path,0,2) == "//");
	}

	private static function getAssetPath($path, $assetsRoot) {
		return self::isExternal($path) ? $path : ($assetsRoot . $path);
	}

	public static function loadDirectly($assetsRoot = '/assets/') {

		foreach(self::$assetList['vendor'] as $vendor) {
			$vendorFile = self::$_vendorLibs[$vendor];
			if(!$vendorFile) { continue; }

			$ext = Utils::fileExtension($vendorFile);

			if($ext == "css") {
				self::_loadCSS(self::getAssetPath($vendorFile, $assetsRoot));
			} else if($ext == "js") {
				self::_loadJS(self::getAssetPath($vendorFile, $assetsRoot));
			} else {
				continue;
			}

		}

		foreach(self::$assetList['df3'] as $dieselLib) {
			$dieselFile = self::$_dieselLibs[$dieselLib];
			if(!$dieselFile) { continue; }

			$ext = Utils::fileExtension($dieselFile);
			switch($ext) {

				case "css":
					self::_loadCSS(self::getAssetPath($dieselFile, $assetsRoot));
					break;

				case "js": default:
					self::_loadJS(self::getAssetPath($dieselFile, $assetsRoot));
					break;

			}
		}

		foreach(self::$assetList['css'] as $cssFile) {
			self::_loadCSS(self::getAssetPath($cssFile, $assetsRoot));
		}

		foreach(self::$assetList['js'] as $jsFile) {
			self::_loadJS(self::getAssetPath($jsFile, $assetsRoot));
		}

	}

	public static function bootstrap() {

		ob_start();

		self::_bootstrapJS();

		if(self::$cacheFlag) {

			$isCompressedStr = ((self::$compressFlag)?"compressed":"packed");

			self::$cacheHashCSS = "{$isCompressedStr}_" . md5(self::$computedMtime['css']."-".self::$numFiles['css']); //md5(serialize(self::$assetList));
			self::$cacheHashJS = "{$isCompressedStr}_" . md5(self::$computedMtime['js']."-".self::$numFiles['js']); //md5(serialize(self::$assetList));

			if(!self::$cacheExternalFlag) {
				foreach(self::$assetList['vendor'] as $vendor) {
					$vendorFile = self::$_vendorLibs[$vendor];
					if(!$vendorFile) { continue; }
					self::_loadJS($vendorFile);
				}

				foreach(self::$assetList['css'] as $cssFile) {
					if(self::isExternal($cssFile)) {
						self::_loadCSS($cssFile);
					}
				}

				foreach(self::$assetList['js'] as $jsFile) {
					if(self::isExternal($jsFile)) {
						self::_loadJS($jsFile);
					}
				}
			}

			self::_loadCachedJS();
			self::_loadCachedCSS();

		} else {

			foreach(self::$assetList['vendor'] as $vendor) {
				$vendorFile = self::$_vendorLibs[$vendor];
				if(!$vendorFile) { continue; }

				$ext = Utils::fileExtension($vendorFile);

				if($ext == "css") {
					self::_loadCSS($vendorFile);
				} else if($ext == "js") {
					self::_loadJS($vendorFile);
				} else {
					continue;
				}


			}

			foreach(self::$assetList['df3'] as $dieselLib) {
				$dieselFile = self::$_dieselLibs[$dieselLib];
				if(!$dieselFile) { continue; }

				$ext = Utils::fileExtension($dieselFile);
				switch($ext) {

					case "css":
						self::_loadCSS($dieselFile);
						break;

					case "js": default:
					self::_loadJS($dieselFile);
					break;

				}
			}

			foreach(self::$assetList['js'] as $jsFile) {
				self::_loadJS($jsFile);
			}

			foreach(self::$assetList['css'] as $cssFile) {
				self::_loadCSS($cssFile);
			}

			foreach(self::$assetList['scss'] as $scssFile) {
				self::_loadCSS($scssFile);
			}

		}

		$bootstrap = ob_get_contents();
		ob_end_clean();

		return $bootstrap;

	}

	public static function _loadCachedCSS() {
		$cachedCSSFile = 'assets/cached/css_'.self::$cacheHashCSS.'.css';

		if(!file_exists($cachedCSSFile)) {
			self::_generateCSSCache($cachedCSSFile);
		}

		self::_loadCSS($cachedCSSFile);
	}

	public static function _generateCSSCache($cacheFile) {

		$buffer = "";

		foreach(self::$assetList['df3'] as $dieselLib) {
			$dieselFile = self::$_dieselLibs[$dieselLib];
			if(!$dieselFile) { $buffer .= "\n\n/* DF3 CSS FILE NOT FOUND: {$dieselLib} */"; continue; }

			$dieselFile = DF3_PATH . "assets/css/" . basename($dieselFile);

			$ext = Utils::fileExtension($dieselFile);
			if($ext != "css") { continue; }

			$contents = file_get_contents($dieselFile);
			if(self::$compressFlag) {
				$contents = self::_compressCSS($contents);
			}

			$buffer .= "\n\n/* DF3 CSS: {$dieselLib}  */\n" . $contents;

		}

		foreach(self::$assetList['css'] as $cssFile) {

			$cssFile = APP_PATH . "../assets/" . $cssFile;

			if(!self::$cacheExternalFlag && substr($cssFile, 0, 7) == "http://") {
				continue;
			}

			if(!file_exists($cssFile)) { $buffer .= "\n\n/* CSS FILE NOT FOUND: {$cssFile} */";continue; }

			$contents = file_get_contents($cssFile);
			if(self::$compressFlag) {
				$contents = self::_compressCSS($contents);
			}

			$buffer .= "\n\n/* CSS: {$cssFile} */\n" . $contents;

		}

		foreach(self::$assetList['scss'] as $scssFile) {

			$scssFile = APP_PATH . "../assets/" . $scssFile;

			if(!file_exists($scssFile)) { $buffer .= "\n\n/* SCSS FILE NOT FOUND: {$scssFile} */";continue; }

			$parser = new SassParser(array(
				'style' => 'expanded',
				'cache' => FALSE,
				'syntax' => 'scss',
				'debug' => FALSE,
				'callbacks' => array(
					'warn' => 'warn',
					'debug' => 'debug'
				),
			));

			$contents = $parser->toCss($scssFile);
			if(self::$compressFlag) {
				$contents = self::_compressCSS($contents);
			}

			try {
				$buffer .= "\n\n/* SCSS: {$scssFile} */\n" . $contents;
			} catch (Exception $e) {
				$buffer .= "\n/* [!] FAILED: ".$e->getMessage()." */";
			}


		}

		$buffer .= "\n";

		file_put_contents($cacheFile, $buffer);

	}

	public static function _loadCSS($file) {
		if(self::$forceCacheSkip) {
			$file .= "?NC=".time();
		}
		echo " <link rel=\"stylesheet\" type=\"text/css\" href=\"{$file}\" />\n";
	}

	public static function _loadCachedJS() {
		$cachedJSFile = 'assets/cached/js_'.self::$cacheHashJS.'.js';

		if(!file_exists($cachedJSFile)) {
			self::_generateJSCache($cachedJSFile);
		}

		self::_loadJS($cachedJSFile);
	}

	public static function _generateJSCache($cacheFile) {

		$buffer = "";

		if(self::$cacheExternalFlag) {
			foreach(self::$assetList['vendor'] as $vendor) {
				$vendorFile = self::$_vendorLibs[$vendor];
				if(!$vendorFile) { continue; }

				$ext = Utils::fileExtension($vendorFile);
				if($ext != "js") { continue; }

				if(substr($vendorFile,0,2) == "//") {
					$vendorFile = "http:" . $vendorFile;
				}

				$contents = Utils::getURLContents($vendorFile);
				if(self::$compressFlag) {
					$contents = self::_compressJS($contents);
				}

				$buffer .= "\n\n/* External Library: {$vendor} (from {$vendorFile}) */\n" . $contents;

			}
		}

		foreach(self::$assetList['df3'] as $dieselLib) {
			$dieselFile = self::$_dieselLibs[$dieselLib];
			if(!$dieselFile) { $buffer .= "\n\n/* DF3 JS FILE NOT FOUND: {$dieselLib} */";continue; }

			$dieselFile = DF3_PATH . "assets/js/" . basename($dieselFile);

			$ext = Utils::fileExtension($dieselFile);
			if($ext != "js") { continue; }

			$contents = file_get_contents($dieselFile);
			if(self::$compressFlag) {
				$contents = self::_compressJS($contents);
			}

			$buffer .= "\n\n/* DF3 Library: {$dieselLib}  */\n" . $contents;

		}


		foreach(self::$assetList['js'] as $jsFile) {

			$jsFile = APP_PATH . "../assets/" . $jsFile;

			if(!file_exists($jsFile)) { $buffer .= "\n\n/* JS FILE NOT FOUND: {$jsFile} */";continue; }

			$contents = file_get_contents($jsFile);
			if(self::$compressFlag) {
				$contents = self::_compressJS($contents);
			}

			$buffer .= "\n\n/* JS: {$jsFile} */\n" . $contents;

		}

		$buffer .= "\n";

		file_put_contents($cacheFile, $buffer);

	}

	public static function _loadJS($file) {
		if(self::$forceCacheSkip) {
			$file .= "?NC=".time();
		}
		echo " <script type=\"text/javascript\" src=\"{$file}\"></script>\n";
	}


	public static function _compressCSS($in) {
		$out = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $in);
		$out = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $out);
		$out = str_replace('{ ', '{', $out);
		$out = str_replace(' }', '}', $out);
		$out = str_replace('; ', ';', $out);

		return $out;
	}

	public static function _compressJS($in) {
		return JSMin::minify($in);
	}

	public static function _bootstrapJS() {
		$anchorPath = addslashes(APP_ANCHOR_PATH);
		echo "\n <script type=\"text/javascript\">var _DF3_ANCHOR='{$anchorPath}';function anchor(i){return _DF3_ANCHOR+i;};</script>\n";
	}


}