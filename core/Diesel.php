<?php
/**
 * Diesel Framework
 * Copyright © LQDI Technologies - 2012
 * http://www.lqdi.net
 *
 * Framework master controller
 * Controlador-mestre do framework
 *
 * @author Aryel Tupinambá
 */

class Diesel {

	const NAMING_SCHEME_LEGACY = 1;
	const NAMING_SCHEME_FIXED = 2;

	public static $classNamingScheme = Diesel::NAMING_SCHEME_LEGACY;

    public static $context = null;
    public static $controllers = array();


    /**
     * Inicializa os autoloaders do framework
     *
     * @static
     * @return void
     */
    public static function init() {

        spl_autoload_register(array('Diesel', 'loadClass'));

    }

	/**
	 * Seta o framework para o modo corrigido de nomes de classe (ClassName -> class_name.php)
	 */
	public static function useFixedNamingScheme() {
		self::$classNamingScheme = self::NAMING_SCHEME_FIXED;
	}

	/**
	 * Seta o framework para o modo legacy de nomes de classes
	 */
	public static function useLegacyNamingScheme() {
		self::$classNamingScheme = self::NAMING_SCHEME_LEGACY;
	}

	/**
	 * Retorna o nome do arquivo de uma determinada classe
	 * @param string $class O nome da classe a ser carregada
	 * @return string O nome do arquivo esperado
	 */
	public static function getClassFileName($class) {

		switch(self::$classNamingScheme) {
			case self::NAMING_SCHEME_LEGACY:
				return ucfirst(basename($class)).".php";
			default:
			case self::NAMING_SCHEME_FIXED:
				return Utils::camelcaseToUnderline(basename($class)).".php";
		}

	}

	/**
	 * Retorna a rota/URI solicitada pela URL ou pelos parâmetros CLI
	 * @return string O caminho/URI solicitada
	 */
	public static function getRequestedRoute() {

		if(defined('CLI_MODE')) { // CLI route (php -f index.php controller method param1 param2...)
			global $argv;
			return join("/", array_slice($argv,1));
		}

		if(isset($_GET['DIESEL_ROUTE'])) { // Rewritten URL route (lqdi.net/controller/method/params)
			return $_GET['DIESEL_ROUTE'];
		}

		if(isset($_SERVER['REQUEST_URI'])) { // Request URI (via nginx, lqdi.net/controller/method/parameters)
			return substr($_SERVER['REQUEST_URI'], 1);
		}

		if(isset($_SERVER['PATH_INFO'])) { // Path Info route (lqdi.net/index.php/controller/method/params)
			return substr($_SERVER['PATH_INFO'], 1);
		}

		// Query String route (lqdi.net/index.php?controller/method/params)
		return $_SERVER['QUERY_STRING'];

	}

    /**
     * Carrega a classe de um controlador.
     * Armazena as instâncias de forma a não duplicar.
     *
     * @static
     * @param string $moduleName O nome do módulo
     * @param string $class O nome da classe do controlador
     *
     * @return mixed Uma instância da classe
     */
    public static function &loadController($moduleName, $class) {

		$isLegacy = self::$classNamingScheme == self::NAMING_SCHEME_LEGACY;

        $controllerID = $moduleName."::".$class; // Unique module+controller identifier

        // If we already loaded this controller before, return the existing instance
        if (isset(self::$controllers[$controllerID])) {
            return self::$controllers[$controllerID];
        }

        // Load the controller according to the current context
        if($moduleName == "app") {
            $controllerPath = DF3_PATH . "../" . APP_CONTROLLERS_FOLDER . self::getClassFileName($class);
        } else {
            $module = ModuleManager::get($moduleName);
            $controllerPath = DF3_PATH . "../" . APP_MODULES_FOLDER . $module->folderName . "/controllers/" . self::getClassFileName($class);
        }

        // Check if controller file exists
        if(!file_exists($controllerPath)) {
            error("Error 404-CF: Could not locate the controller class path in the current/specified context. Module: [{$moduleName}], Class: [{$class}], Path: [{$controllerPath}]");
        }

        // Includes the file
        include($controllerPath);


		if($isLegacy) {
			$class = ucfirst($class);
		} else {
			$class = Utils::underlineToCamelcase($class);
		}

        // Check if the controller class exists
        if(!class_exists($class)) {
            error("Error 404-CC: The specified controller class does not exist in the current/specified context. Module: [{$moduleName}], Class: [{$class}]");
        }

        // Creates controller instance and stores it in the registry
        $controllers[$controllerID] = new $class();

        // Returns the instance
        return $controllers[$controllerID];

    }

    /**
     * Executa um método de um controlador
     *
     * @static
     * @param array $path Um caminho da aplicação. Array associativa com os campos: module, controller, method e parameters
     *
     * @return mixed O retorno da função
     */

    public static function execute($path) {

        $controllerPrefix = config('CONTROLLER_PREFIX');
        $controllerSuffix = config('CONTROLLER_SUFFIX');

        // Load the corresponding controller
        $instance =& Diesel::loadController($path['module'], $controllerPrefix.$path['controller'].$controllerSuffix);

        // Check if the called method actually exists
        if(!method_exists($instance, $path['method'])) {

			// If controller has the catch-all "handle_request" method, call that instead
			if(method_exists($instance, "handle_request")) {
				array_unshift($path['parameters'], $path['method']);
				return call_user_func_array( array($instance, "handle_request"), $path['parameters']);
			} else {
				error("Error 101: The specified controller does not support the specified method. Module: [{$path['module']}], Controller: [{$path['controller']}], Method: [{$path['method']}]");
			}

        }

        // Calls the instance method with the defined parameters, and returns the result
        return call_user_func_array( array($instance, $path['method']), $path['parameters']);
    }

    /**
     * Carrega uma classe, buscando primeiro na pasta da aplicação e
     * depois consultando o registro de módulos pelo primeiro módulo que
     * contem esse model.
     *
     * Essa função é chamada automaticamente quando uma classe é instanciada
     * sem que uma definição seja encontrada (spl_register_autoload).
     *
     * @static
     * @param string $className O nome do model
     */
    public static function loadClass($className) {

	    // Checks the include paths for the class first
	    $path = join("/",explode("_", str_replace(".","",$className))).".php";

	    if(stream_resolve_include_path($path)) {
		    include($path);
		    return;
	    }

	    // Looks for a model in the app
        $modelContext = "app";

        if(self::$context != "app") {
            $module = ModuleManager::getCurrent();

            if($module) {
                $path = DF3_PATH . "../" . APP_MODULES_FOLDER . $module->folderName . "/models/" . self::getClassFileName($className);
                if(file_exists($path)) {
                    $modelContext = self::$context;
                } else {
                    $modelContext = "app";
                }
            } else {
                $modelContext = "app";
            }

        }

        self::loadModelFromModule($modelContext, $className);

    }

	/**
	 * @deprecated
	 * @see Diesel::loadClass
	 * @param $modelName
	 */
	public static function loadModel($modelName) {
		self::loadClass($modelName);
	}

    /**
     * Carrega a configuração de um módulo
     *
     * @static
     * @param string $moduleFolder A pasta no qual o módulo está localizado
     * @return Module
     */
    public static function loadModule($moduleFolder) {

        $path = APP_MODULES_FOLDER . basename($moduleFolder) . "/mod.config.php";

        if(!file_exists($path)) {
            error("Error 103-A: Could not load module in folder '{$moduleFolder}', config file was not found");
        }

        include($path);

        return Module::get($moduleFolder);

    }

    /**
     * Carrega uma classe de um model dentro de um módulo especificado
     *
     * @static
     * @param string $moduleName O nome do módulo
     * @param string $modelName O nome do model
     */
    public static function loadModelFromModule($moduleName, $modelName) {

        if($moduleName == "app") {
            $path = DF3_PATH . "../" . APP_MODELS_FOLDER . self::getClassFileName($modelName);

            if( file_exists($path) ) {
                include($path);
            } else {
                error("Error 102-A: Cannot load model [{$modelName}] from application context; the model class does not exist");
            }

        } else {

            $module = ModuleManager::get($moduleName);

            if(!$module) {
                error("Error 102-B: Cannot load model [{$modelName}] from module [{$moduleName}]; the context does not exist");
            }

            $path = DF3_PATH . "../" . APP_MODULES_FOLDER . $module->folderName . "/models/" . self::getClassFileName($modelName);

            if(file_exists($path)) {
                include($path);
            } else {
                error("102-C: Cannot load model [{$modelName}] from context [{$moduleName}]; the model class does not exist in this context");
            }

        }

    }

    /**
     * Carrega múltiplos models dentro de um módulo especificado
     *
     * @static
     * @param string $moduleName O nome do módulo
     * @param array $modelList Uma array com uma lista de models a carregar
     */
    public static function loadModelsFromModule($moduleName, $modelList = array()) {

        foreach($modelList as $modelName) {

            Diesel::loadModelFromModule($moduleName, $modelName);

        }

    }

    /**
     * Carrega uma tela de visualização, passando os parametros.
     *
     * @static
     * @param string $viewName Nome da view
     * @param array $viewData Parâmetros da view
     * @param string $context Buscar a view em qual contexto? "auto" utiliza o contexto atual.
     * @param string $contentType O tipo MIME de saída
     * @param string $encoding O tipo de codificação da saída
     */
    public static function loadView($viewName, $viewData = array(), $context = "auto", $contentType = "text/html", $encoding="utf-8") {

        if($context == "auto") {
            $context = Diesel::$context;
        }

        if($context == "app") {
            $viewPath = DF3_PATH . "../" . APP_VIEWS_FOLDER;
        } else {
            $module = ModuleManager::get($context);
            $viewPath = DF3_PATH . "../" . APP_MODULES_FOLDER . $module->folderName . "/views";
        }

        // Define variáveis locais para cada item do data
        foreach ($viewData as $varName => $value) {
            $$varName = $value;
        }

        // Liga o buffer de saída
        ob_start();

        if(!headers_sent()) {
            header("Content-Type: {$contentType}; charset={$encoding}");
        }

        // Inclui a view
        include($viewPath . "/" . $viewName . ".php");

        // Joga o buffer de saída na tela
        ob_end_flush();
    }

	/**
	 * Carrega um componente de tela de visualização, passando os parametros.
	 *
	 * @static
	 * @param string $viewComponentName Nome do view component
	 * @param array $viewData Parâmetros do view component
	 * @param string $context Buscar o view component em qual contexto? "auto" utiliza o contexto atual.
	 */
	public static function loadViewComponent($viewComponentName, $viewData = array(), $context = "auto") {
		if($context == "auto") {
			$context = Diesel::$context;
		}

		if($context == "app") {
			$viewPath = DF3_PATH . "../" . APP_VIEWS_FOLDER;
		} else {
			$module = ModuleManager::get($context);
			$viewPath = DF3_PATH . "../" . APP_MODULES_FOLDER . $module->folderName . "/views";
		}

		// Define variáveis locais para cada item do data
		foreach ($viewData as $varName => $value) {
			$$varName = $value;
		}

		// Liga o buffer de saída
		ob_start();

		// Inclui a view
		include($viewPath . "/" . $viewComponentName . ".php");

		// Joga o buffer de saída na tela
		ob_end_flush();
	}

    /**
     * Retorna o HTML do bootstrap do módulo, para ser colocado no <head> da página
     *
     * @static
     * @param string $moduleName O nome do módulo
     * @return string O HTML do bootstrap
     */
    public static function getModuleBootstrap($moduleName) {
        return ModuleManager::get($moduleName)->getBootstrap();
    }


    /**
     * Limpa a memória ocupada pelo framework, apagando as instâncias de controladores carregados
     *
     * @static
     */

    public static function cleanup() {

        foreach(self::$controllers as $controller) {
            unset($controller);
        }

    }

}
