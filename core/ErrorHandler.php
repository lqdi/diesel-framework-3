<?php
/**
 * Diesel Framework
 * Copyright © LQDI Technologies - 2013
 * http://www.lqdi.net
 *
 * Error management and debugging
 * Gerenciador de erros e debugging
 *
 * @author Aryel Tupinambá
 */


/**
 * Mostra uma mensagem de erro
 *
 * @param string $msg Mensagem de erro
 * @param boolean $internal [optional] É uma mensagem de erro interna do PHP?
 * @param Exception $e [optional] O objeto exceção que causou o erro
 *
 * @throws Exception
 */

define('ERROR_HANDLING_DEFAULT', 1);
define('ERROR_HANDLING_CLI', 2);
define('ERROR_HANDLING_EXCEPTION', 3);
define('ERROR_HANDLING_JSON', 4);

function error($msg, $statusCode = "UNKNOWN_ERROR", $internal = false, Throwable $e = NULL, $handlingMode = null) {

	$errorMode = ($handlingMode != null) ? $handlingMode : getErrorHandlingMode();
	if(!$errorMode) { $errorMode = ERROR_HANDLING_DEFAULT; }

    if($e == NULL) {
        if (!$internal) {
            $errorMessage = Utils::HTML($msg);
        } else {
            $errorMessage = $msg;
        }
    } else {
        $errorMessage = "Exception (code: {$statusCode}): {$e->getMessage()}";
    }

    $errorDate = date("c");

    if($e == NULL) {
		$stackTrace = debug_backtrace();
    } else {
		$stackTrace = $e->getTrace();
	}

	$stackTrace = array_reverse($stackTrace);

	if(defined("APP_ABSOLUTE_PATH")) {
		foreach($stackTrace as &$s) {
			$s['file'] = str_replace(APP_ABSOLUTE_PATH, '', $s['file']);
		}
	}

	$errorScript = $stackTrace[0]['file'];
	$errorLine = $stackTrace[0]['line'];

    if(class_exists("Log")) {
        if (sizeof(Log::$logwrites) > 0) {
            $runtimeLog = Log::$logwrites;
        } else {
            $runtimeLog = false;
		}
    } else {
		$runtimeLog = false;
	}

	switch($errorMode) {

		case ERROR_HANDLING_EXCEPTION:
			if($e != null) {
				throw $e;
			} else {
				throw new Exception($msg);
			}

			break;

		case ERROR_HANDLING_CLI:

			echo "\n------------";
			echo "\n[!] ERROR: {$msg}";
			echo "\n- Exception: ".print_r($e, true);

			if(isErrorDebugging()) {
				echo "\n- Backtrace:";
				foreach($stackTrace as $k => $v) {
					echo "\n\t[ {$k} ] => {";
					foreach($v as $i => $j) {
						echo " '{$i}': '{$j}' ";
					}
					echo "}";

				}
			}

			echo "\n\nEnvironment: " . ENVIRONMENT;
			echo "\nPowered by Diesel Framework 3";
			echo "\n------------";
			exit();

			break;

		case ERROR_HANDLING_JSON:

			$errorData = array(
				'message' => $errorMessage,
				'script' => $errorScript,
				'line' => $errorLine,
				'date' => $errorDate
			);

			if(isErrorDebugging()) {

				$errorData['stack'] = $stackTrace;
				$errorData['log'] = $runtimeLog;
				$errorData['post'] = $_POST;
				$errorData['get'] = $_GET;
				$errorData['session'] = $_SESSION;
				$errorData['cookie'] = $_COOKIE;
				$errorData['files'] = $_FILES;
				$errorData['exception'] = $e ? $e : null;

			}

			header("HTTP/1.1 500 Internal server error");
			reply($statusCode, $errorData);
			break;

		case ERROR_HANDLING_DEFAULT: default:

			$outputBuffer = htmlentities(ob_get_contents());

			discard_output();

			header("HTTP/1.1 500 Internal server error");

			echo "<!--#UNEXPECTED_OUTPUT_END#--!>";

			$errorFile = (is_string(config('ERROR_FILE'))) ? config('ERROR_FILE') : DF3_PATH . "assets/default.error.php";
			include($errorFile);

			ob_end_flush();

			exit();

			break;



	}



}

/**
 * Sets the error handling mode
 * @param int $mode The handling mode. Options are:
 *
 * define('ERROR_HANDLING_DEFAULT', 1);
 * define('ERROR_HANDLING_CLI', 2);
 * define('ERROR_HANDLING_EXCEPTION', 3);
 * define('ERROR_HANDLING_JSON', 4);
 */

function setErrorHandling($mode) {
	global $config;
	$config['ERROR_HANDLING_MODE'] = $mode;
}

/**
 * Returns the current error handling mode
 * @return int
 */
function getErrorHandlingMode() {
	global $config;
	return ($config['ERROR_HANDLING_MODE']) ? $config['ERROR_HANDLING_MODE'] : ERROR_HANDLING_DEFAULT;
}

function isErrorDebugging() {
	global $config;
	return ($config['ERROR_DEBUGGING'] === true);
}

/**
 * ATENÇÃO: Função interna do framework, não a chame diretamente! Utilize em seu lugar a função error()
 * Recebe todos os erros não-fatais do PHP e os processa usando o handler de erros do framework
 *
 * @param $errno
 * @param $errstr
 * @param $errfile
 * @param $errline
 * @return bool
 */
function df3_error_handler($errno, $errstr, $errfile, $errline) {

    error("Erro de PHP #{$errno}: {$errstr} (em {$errfile}:{$errline})", "PHP_ERROR", true);

    return true;
}

/**
 * ATENÇÃO: Função interna do framework, não a chame diretamente! Utilize em seu lugar a função error()
 * Recebe todas as exceções não coletadas e as processa usando o handler de erros do framework
 *
 * @param Exception $e
 */
if(!interface_exists('Throwable')) {

	interface Throwable {
		/* @return string */ public function getMessage();
		/* @return int */ public function getCode();
		/* @return string */ public function getFile();
		/* @return int */ public function getLine();
		/* @return array */ public function getTrace();
		/* @return string */ public function getTraceAsString();
		/* @return Throwable */ public function getPrevious();
		/* @return string */ public function __toString();
	}

}

function df3_exception_handler(Throwable $e) {
	error($e->getMessage(), "PHP_EXCEPTION", false, $e);
}

set_error_handler("df3_error_handler", E_ERROR | E_WARNING | E_CORE_ERROR | E_CORE_WARNING | E_PARSE | E_COMPILE_ERROR | E_COMPILE_WARNING | E_USER_ERROR | E_USER_WARNING);
set_exception_handler("df3_exception_handler");
