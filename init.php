<?php
/**
 * Diesel Framework
 * Copyright © LQDI Technologies - 2011
 * http://www.lqdi.net
 *
 * @author Aryel Tupinambá
 */

if(!defined('DF3_PATH')) {
	define('DF3_IS_DEFAULT_PATH', true);
    define('DF3_PATH', './');
}

define('DF3_VERSION', '3.0a');

include(DF3_PATH . "global_functions.php");

include(DF3_PATH . "core/ErrorHandler.php");
include(DF3_PATH . "core/Utils.php");
include(DF3_PATH . "core/Log.php");
include(DF3_PATH . "core/Router.php");
include(DF3_PATH . "core/ModuleManager.php");
include(DF3_PATH . "core/AssetManager.php");
include(DF3_PATH . "core/Diesel.php");

Diesel::init();

$config = array();

define('APP_PATH', DF3_PATH . "../app/");
define('LIB_PATH', DF3_PATH . "../lib/");

if(defined('APP_CONFIG_PATH')) {
	include(APP_CONFIG_PATH . "app.config.php");
} else {
	include(APP_PATH . "app.config.php");
}


define("DIESEL_CONFIG_LOADED", true);

// Sets up error reporting according to error level
switch(ERROR_LEVEL) {

	case "production":
		ini_set('display_errors', 'off');
		error_reporting(0);
		break;
	case "testing":
		ini_set('display_errors', 'on');
		error_reporting(E_ALL ^ (E_ERROR | E_WARNING | E_NOTICE | E_DEPRECATED));
		break;
	case "development":
		ini_set('display_errors', 'on');
		error_reporting(E_ALL ^ (E_WARNING | E_NOTICE | E_DEPRECATED));
		break;
	case "debug":
		ini_set('display_errors', 'on');
		error_reporting(E_ALL ^ (E_NOTICE | E_DEPRECATED));
		break;
	default: case "everything":
		ini_set('display_errors', 'on');
		error_reporting(E_ALL);
		break;

}

$modBaseDir = (defined('APP_MODULES_FOLDER')) ? APP_MODULES_FOLDER : '';

// Registers all required modules
if(is_array($config['REQUIRED_MODULES'])) {
	foreach($config['REQUIRED_MODULES'] as $requiredModule) {

		$modCfgFile = $modBaseDir . "modules/{$requiredModule}/mod.config.php";

		if(file_exists($modCfgFile)) {
			include($modCfgFile);
		}
	}
}

// Loads all required libraries
if(is_array($config['REQUIRED_LIBRARIES'])) {
	foreach($config['REQUIRED_LIBRARIES'] as $requiredLibrary) {
		load_library($requiredLibrary);
	}
}