<?php
/**
 * Diesel Framework
 * Copyright © LQDI Technologies - 2013
 * http://www.lqdi.net
 *
 * @author Aryel Tupinambá <aryel.tupinamba@lqdi.net>
 */


/**
 * Diesel ORM
 * Entity Manager:
 * 		Provides interaction between data entities and the database.
 */
class EntityManager {

	/**
	 * @var \PDO The internal PDO object.
	 */
	private static $pdo = null;

	/**
	 * @var array Server configuration settings.
	 */
	private static $serverSettings;

	/**
	 * @var array Information about the last failed query (any query that failed or returned no results).
	 */
	private static $_lastFailure = array();

	/**
	 * @var bool Keeps track if the application has setup a database configuration or not
	 */
	private static $_isSetup = false;

	/**
	 * @var bool Throw exceptions on failures?
	 */
	private static $_throwExceptions = true;

	/**
	 * Sets up the ORM connection settings.
	 * @param array $serverSettings
	 */
	public static function setup($serverSettings) {
		self::$serverSettings = $serverSettings;
		self::$_isSetup = true;
	}

	/**
	 * Gets the current PDO connection, creating one if it doesn't exist.
	 * @return \PDO The PDO connection
	 */
	public static function getConnection() {

		if(self::$_isSetup == false) {
			error("ORM connection was not setup");
		}

		$server = self::$serverSettings;

		if(self::$pdo === null) {
			try {
				self::$pdo = new \PDO("{$server['driver']}:host={$server['hostname']};port={$server['port']};dbname={$server['database']}", $server['username'], $server['password'], array(\PDO::ATTR_PERSISTENT => $server['isPersistent']));
			} catch (\PDOException $e) {
				self::handleException("connect", $e);
				return false;
			}

			self::$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_SILENT);
			self::$pdo->exec("SET CHARACTER SET utf8");
		}

		return self::$pdo;

	}

	/**
	 * Handles an internal exception.
	 * @param string $operation
	 * @param \Exception $e
	 *
	 * @throws EntityManagerException
	 * @throws \Exception
	 */
	private static function handleException($operation, \Exception $e) {
		self::registerFailure($operation, new PDOStatement(), $e);
	}

	/**
	 * Registers a query failure, so it may be queried from outside the ORM.
	 * @param string $operation The operation that failed.
	 * @param PDOStatement $statement The PDO statement executed.
	 * @param \Exception $ex The failure exception, if it exists
	 *
	 * @throws EntityManagerException
	 * @throws Exception
	 */
	private static function registerFailure($operation, PDOStatement $statement, \Exception $ex = null) {
		self::$_lastFailure = array(
			'operation' => $operation,
			'statement' => $statement
		);

		if(self::$_throwExceptions) {
			if($ex !== null) {
				throw $ex;
			} else {
				throw new EntityManagerException($operation, $statement);
			}
		}
	}

	/**
	 * Clears the last query failure. Run this when a query has succeeded.
	 */
	private static function clearFailure() {
		self::$_lastFailure = null;
	}

	/**
	 * Gets failure information from the last executed query. If the query succeeded, it returns null.
	 * @return array|null
	 */
	public static function getLastFailure() {
		return self::$_lastFailure;
	}

	public static function getLastFailureMessage() {
		if(!self::$_lastFailure) {
			return false;
		}

		$stmt = self::$_lastFailure['statement'];
		if(!$stmt) {
			return "No statement executed on operation: ".self::$_lastFailure['operation'];
		}

		return join("/", self::$_lastFailure['statement']->errorInfo());
	}

	/**
	 * Checks if the previous executed query has failed or not.
	 * @return bool
	 */
	public static function hasFailed() {
		return (self::$_lastFailure === null);
	}

	// ---------------------------------------------------------------------------------------------

	/**
	 * Injects data from an Entity object inside a prepared statement. Useful for inserts and updates.
	 * @param Entity $ent The entity whose data is being imported.
	 * @param \PDOStatement $stmt The statement that will receive the data.
	 * @param array|null $fieldList A list of fields to import, or null to import all entity persistent fields.
	 */
	private static function injectObjectIntoStatement(Entity $ent, \PDOStatement $stmt, $fieldList = null) {

		if(!is_array($fieldList)) {
			$fieldList = $ent->getEntityDefinition()->persistentFields;
		}

		foreach($fieldList as $field) {
			$ent->bindPropertyToStatement($stmt, $field);
		}

	}

	/**
	 * Prepares a string containing a list of key-value pairs ready to receive data from a prepared statement.
	 * @param array $fields A list of fields.
	 * @param string $glue The glue to use (defaults to ", ").
	 * @return string
	 */
	private static function getKeyValuePairStatement($fields, $glue = ", ") {

		$stmt = array();

		foreach($fields as $field) {
			array_push($stmt, "`{$field}` = :{$field}");
		}

		$stmt = join(" {$glue} ", $stmt);

		return $stmt;

	}

	/**
	 * Gets a list of field values in prepared statement format
	 * @param array $fields A list of fields
	 * @return string
	 */
	private static function getFieldListStatement($fields) {

		$stmtFields = array();
		$stmtValues = array();

		foreach($fields as $field) {
			array_push($stmtFields, "`{$field}`");
			array_push($stmtValues, ":{$field}");
		}

		$stmtFields = join(", ", $stmtFields);
		$stmtValues = join(", ", $stmtValues);

		$stmt = "({$stmtFields}) VALUES ({$stmtValues})";

		return $stmt;

	}

	/**
	 * Prepares a string containing ORDER and LIMIT settings to a query, according to a settings array.
	 * @param array $settings
	 * @return string
	 */
	private static function getSelectSettings($settings) {

		$orderStr = (isset($settings['order'])) ? "ORDER BY {$settings['order']}" : "";
		$limitStr = (isset($settings['limit'])) ? "LIMIT {$settings['limit']}" : "";

		$settingsStr = "{$orderStr} {$limitStr}";

		return $settingsStr;

	}

	// ---------------------------------------------------------------------------------------------

	/**
	 * Updates an entity already in the database.
	 * @param Entity $ent The entity.
	 * @param array $fieldList The list of fields to update. Defaults to all persistent ones.
	 * @return bool
	 * @throws Exception|EntityManagerException|PDOException
	 */
	public static function entityUpdate(Entity $ent, $fieldList = null) {

		$pdo = self::getConnection();

		$ent->beforeSave();
		$ent->beforeUpdate();

		if($fieldList === null) {
			$fieldList = $ent->getEntityDefinition()->persistentFields;
		} else {

			if(!is_array($fieldList)) {
				throw new Exception("ORM\\EntityManager::entityUpdateParameter -> $fieldList must be array or null");
			}

			if(!in_array("id", $fieldList)) {
				array_push($fieldList, "id");
			}

		}

		$table = $ent->getEntityDefinition()->tableName;
		$kvList = self::getKeyValuePairStatement($fieldList, ", ");

		$stmt = $pdo->prepare("UPDATE {$table} SET {$kvList} WHERE ( `id` = :id )");
		self::injectObjectIntoStatement($ent, $stmt, $fieldList);

		$status = $stmt->execute();

		if(!$status) {
			self::registerFailure("entityUpdate", $stmt);
			return false;
		} else {
			$ent->afterUpdate();
			$ent->afterSave();
			self::clearFailure();
			return true;
		}

	}

	/**
	 * Inserts an entity in the database.
	 * @param Entity $ent The entity to insert.
	 * @return int The entity ID.
	 */
	public static function entityInsert(Entity $ent) {

		$pdo = self::getConnection();

		$ent->beforeSave();
		$ent->beforeInsert();

		$fieldList = $ent->getEntityDefinition()->persistentFields;
		$table = $ent->getEntityDefinition()->tableName;

		$fieldListStmt = self::getFieldListStatement($fieldList);

		$stmt = $pdo->prepare("INSERT INTO {$table} {$fieldListStmt}");

		self::injectObjectIntoStatement($ent, $stmt, $fieldList);

		$status = $stmt->execute();

		if(!$status) {
			self::registerFailure("entityInsert", $stmt);
			return false;
		} else {
			self::clearFailure();
			$entityID = $pdo->lastInsertId();
			$ent->setEntityID($entityID);

			$ent->afterInsert();
			$ent->afterSave();

			return $entityID;
		}

	}

	/**
	 * Checks if an entity exists in the database.
	 * @param EntityDefinition $def The entity class definition.
	 * @param int $entityID The entity ID.
	 * @return bool True if it exists, false if not.
	 */
	public static function entityCheckIfExists(EntityDefinition $def, $entityID) {

		$pdo = self::getConnection();

		$table = $def->tableName;

		$stmt = $pdo->prepare("SELECT id FROM `{$table}` WHERE `id` = :id");
		$status = $stmt->execute(array('id' => $entityID));

		if(!$status) {
			self::registerFailure("entityCheckIfExists", $stmt);
			return false;
		} else {
			self::clearFailure();
			return (bool) intval($stmt->fetchColumn());
		}

	}

	/**
	 * Fetches an entity by it's ID.
	 * @param EntityDefinition $def The entity class definition.
	 * @param int $entityID The entity ID.
	 * @return Entity
	 */
	public static function entityFetchByID(EntityDefinition $def, $entityID) {

		$pdo = self::getConnection();

		$table = $def->tableName;
		$fieldList = join(", ", $def->persistentFields);

		$stmt = $pdo->prepare("SELECT {$fieldList} FROM `{$table}` WHERE `id` = :id");
		$status = $stmt->execute(array('id' => $entityID));

		if(!$status) {
			self::registerFailure("entityFetchByID", $stmt);
			return false;
		} else {
			self::clearFailure();
			return $stmt->fetchObject($def->className, array());
		}

	}

	/**
	 * Finds all entities containing a certain set of attributes.
	 * @param EntityDefinition $def The entity class definition.
	 * @param array $attributes An associative array containing a list of attributes.
	 * @param array $settings Order, limit and operator query settings.
	 * @return Entity[]
	 */
	public static function entityFindByAttributes(EntityDefinition $def, $attributes = null, $settings = array()) {

		$pdo = self::getConnection();

		$table = $def->tableName;
		$fieldList = join(", ", $def->persistentFields);

		$attributeList = ($attributes !== null && sizeof($attributes) > 0) ? " WHERE " . self::getKeyValuePairStatement(array_keys($attributes), (isset($settings['operator'])?$settings['operator']:'AND') ) : "";
		$settingsStr = self::getSelectSettings($settings);

		$stmt = $pdo->prepare("SELECT {$fieldList} FROM `{$table}` {$attributeList} {$settingsStr}");

		$status = $stmt->execute($attributes);

		if(!$status) {
			self::registerFailure("entityFindByAttributes", $stmt);
			return false;
		} else {
			self::clearFailure();
			return $stmt->fetchAll(\PDO::FETCH_CLASS, $def->className, array());
		}

	}

	/**
	 * Finds all entities having a certain set of conditions true.
	 * @param EntityDefinition $def The entity class definition.
	 * @param array $conditionals An array containing a list of conditions.
	 * @param array $settings Order, limit and operator query settings.
	 * @return Entity[]
	 */
	public static function entityFindByConditionals(EntityDefinition $def, $conditionals = null, $settings = array()) {

		$pdo = self::getConnection();

		$table = $def->tableName;
		$fieldList = join(", ", $def->persistentFields);

		$conditionalsStr = ($conditionals !== null) ? " WHERE " . join( (isset($settings['operator'])?" {$settings['operator']} ":' AND '), $conditionals) : "";
		$settingsStr = self::getSelectSettings($settings);

		$stmt = $pdo->prepare("SELECT {$fieldList} FROM `{$table}` {$conditionalsStr} {$settingsStr}");

		$status = $stmt->execute();

		if(!$status) {
			self::registerFailure("entityFindByConditionals", $stmt);
			return false;
		} else {
			self::clearFailure();
			return $stmt->fetchAll(\PDO::FETCH_CLASS, $def->className, array());
		}

	}

	/**
	 * Executes a passive update on several entities at once.
	 * @param EntityDefinition $def The entity class definition.
	 * @param array $conditionals An array containing a list of conditions to be true.
	 * @param array $properties An associative array containing a set of properties to update.
	 * @param string $operator The conditional operator (defaults to AND).
	 * @return bool
	 */
	public static function entityPassiveUpdate(EntityDefinition $def, $conditionals, $properties, $operator = "AND") {

		$pdo = self::getConnection();

		$kvList = self::getKeyValuePairStatement(array_keys($properties), ", ");
		$conditionalsStr = join(" {$operator} ", $conditionals);

		$stmt = $pdo->prepare("UPDATE {$def->tableName} SET {$kvList} WHERE ( {$conditionalsStr} )");

		$status = $stmt->execute($properties);

		if(!$status) {
			self::registerFailure("entityPassiveUpdate", $stmt);
			return false;
		} else {
			self::clearFailure();
			return true;
		}

	}

	/**
	 * Deletes multiple entities at once.
	 * @param EntityDefinition $def The entity class definition.
	 * @param array $conditionals An array containing a list of conditions to be true.
	 * @param string $operator The conditional operator (defaults to AND).
	 * @return bool
	 */
	public static function entityPassiveDelete(EntityDefinition $def, $conditionals, $operator = "AND") {

		$pdo = self::getConnection();

		$conditionalsStr = join(" {$operator} ", $conditionals);

		$stmt = $pdo->prepare("DELETE FROM {$def->tableName} WHERE ( {$conditionalsStr} )");

		$status = $stmt->execute();

		if(!$status) {
			self::registerFailure("entityPassiveDelete", $stmt);
			return false;
		} else {
			self::clearFailure();
			return true;
		}

	}

	/**
	 * Gets the amount of entity instances in the database, grouped by each field.
	 * @param EntityDefinition $def The entity class definition.
	 * @param string $groupField The grouping field.
	 * @param int $maxResults [optional] The max number of groups to return. Omit or 0 for all.
	 * @return array Associative array containing groups as keys and amounts as values.
	 */
	public static function entityGetGroupCount(EntityDefinition $def, $groupField, $maxResults = 0) {

		$pdo = self::getConnection();

		$sql = "SELECT `{$groupField}`, COUNT(`{$groupField}`) as num FROM {$def->tableName} GROUP BY `{$groupField}` ORDER BY num DESC";

		if($maxResults > 0) {
			$sql .= " LIMIT {$maxResults}";
		}

		$stmt = $pdo->query($sql);

		if(!$stmt) {
			self::registerFailure("entityGetCount", $stmt);
			return false;
		}

		$results = $stmt->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_GROUP);

		$stats = array();

		foreach($results as $key => $val) {
			$stats[$key] = intval($val[0]['num']);
		}

		return $stats;

	}

}

class EntityManagerException extends Exception {

	/**
	 * @var string The name of the failed operation
	 */
	public $operation = null;

	/**
	 * @var PDOStatement The PDO statement that was being executed
	 */
	public $statement = null;

	/**
	 * @var string The PDO error message
	 * This is cached in a public variable so the exception can be var_dumped with all information
	 */
	public $errorMessage = null;

	/**
	 * @var string The query parameter dump
	 * This is cached in a public variable so the exception can be var_dumped with all information
	 */
	public $parameterDump = null;

	/**
	 * An exception thrown by the EntityManager
	 *
	 * @param string $operation The operation being executed
	 * @param PDOStatement $statement The PDO statement being executed, or null if none
	 */
	public function __construct($operation, PDOStatement $statement = null) {

		$this->operation = $operation;
		$this->statement = $statement;
		$this->code = "ORM_EXCEPTION";

		if(!$this->statement) {
			$this->message = "[EntityManagerException] (generic) failed to execute primary operation '{$operation}'";
			return;
		}

		$this->errorMessage = $this->getErrorMessage();
		$this->parameterDump = $this->getDebugDump();
		$this->message = "[EntityManagerException] failed to execute statement operation '{$operation}': {$this->getErrorMessage()}";

	}

	/**
	 * Gets the PDO/MySQL error message
	 * If no statement was executed in this failure (ie.: while connecting), the message is "No statement executed"
	 * @return string
	 */
	public function getErrorMessage() {
		if(!$this->statement) {
			return "No statement executed";
		}

		return join("/", $this->statement->errorInfo());
	}

	/**
	 * Gets the PDO debug dump with the executed query and dumped parameters
	 * If no statement was executed in this failure (ie.: while connecting), this function returns null.
	 * @return string
	 */
	public function getDebugDump() {

		if(!$this->statement) {
			return null;
		}

		ob_start();
		$this->statement->debugDumpParams();
		$dump = ob_get_contents();
		ob_end_clean();
		return $dump;
	}

}

abstract class Entity implements ArrayAccess {

	/**
	 * @var int The entity ID. Unique identifier.
	 */
	public $id;

	/**
	 * Sets the unique identifier to this entity.
	 * @param int $id
	 */
	public function setEntityID($id) {
		$this->id = intval($id);
	}

	/**
	 * Permanently sets a key in the entity, updating the database accordingly.
	 * @param string $key The entity key.
	 * @param mixed $value The desired value.
	 */
	public function set($key, $value) {
		$this->$key = $value;
		$this->update(array($key));
	}

	/**
	 * Saves the current data on this entity to the database.
	 * @param array $fieldList An optional list of fields to update. Defaults to all persistent fields.
	 * @return bool
	 */
	public function update($fieldList = null) {
		return EntityManager::entityUpdate($this, $fieldList);
	}

	/**
	 * Inserts the current entity in the database.
	 * @return int The entity unique ID.
	 */
	public function insert() {
		return EntityManager::entityInsert($this);
	}

	/**
	 * Deletes the current entity from the database. Will clear it's internal unique ID.
	 * @return bool
	 */
	public function delete() {
		$status = EntityManager::entityPassiveDelete(static::getEntityDefinition(), array("id = {$this->id}"));
		if($status) {
			$this->id = null;
		}

		return $status;

	}

	/**
	 * Called by EntityManager before an entity is updated/inserted on the database
	 */
	public function beforeSave() { }

	/**
	 * Called by EntityManager before an entity is updated on the database
	 */
	public function beforeUpdate() { }

	/**
	 * Called by EntityManager before an entity is inserted on the database
	 */
	public function beforeInsert() { }

	/**
	 * Called by EntityManager after an entity is updated/inserted on the database
	 */
	public function afterSave() { }

	/**
	 * Called by EntityManager after an entity is updated on the database
	 */
	public function afterUpdate() { }

	/**
	 * Called by EntityManager after an entity is inserted on the database
	 */
	public function afterInsert() { }

	/**
	 * Gets an associative array containing all persistent properties of this entity
	 * @return array
	 */
	public function getPersistentProperties() {
		$data = array();
		foreach($this->getEntityDefinition()->persistentFields as $field) {
			$data[$field] = $this->$field;
		}
		return $data;
	}

	/**
	 * Binds a property of this entity to a certain PDO statement
	 * @param PDOStatement $stmt The PDO statement
	 * @param string $property The property name
	 */
	public function bindPropertyToStatement(PDOStatement $stmt, $property) {
		$stmt->bindParam($property, $this->$property);
	}

	/**
	 * Gets an array of client (public to front-end) variables of this entity.
	 * These will potentially be serialized to JSON or XML.
	 * Defaults to all persistent variables.
	 * @param mixed $queryParameters Optional parameters to filter data.
	 * @return array
	 */
	public function getClientData($queryParameters = null) {
		return $this->extractProperties(array_keys($this->getPersistentProperties()));
	}

	/**
	 * Extracts a list of properties from this entity
	 * @param array $propertyList A list of properties
	 * @return array An associative array containing its values
	 */
	public function extractProperties($propertyList) {
		$data = array();
		foreach($propertyList as $field) {
			$data[$field] = $this->$field;
		}
		return $data;
	}

	// ---------------------------------------------------------------------------------------------

	/**
	 * Checks if an entity with the specified ID exists in the database.
	 * @param int $id The entity's ID.
	 * @return bool True if exists, false if not.
	 */
	public static function checkIfExists($id) {
		return EntityManager::entityCheckIfExists(static::getEntityDefinition(), intval($id));
	}

	/**
	 * Gets a single entity by it's unique ID.
	 * @param int $id The entity's ID.
	 * @return static
	 */
	public static function getByID($id) {
		return EntityManager::entityFetchByID(static::getEntityDefinition(), intval($id));
	}

	/**
	 * Copies an entity by it's unique ID.
	 * @param int $id The original entity ID.
	 * @return static The new, cloned entity.
	 */
	public static function copyFromID($id) {
		$original = EntityManager::entityFetchByID(static::getEntityDefinition(), intval($id));
		$new = clone $original;
		$new->id = null;
		$new->insert();
		return $new;
	}

	/**
	 * Deletes an entity from the database by it's ID.
	 * @param int $id The entity's unique ID.
	 * @return bool
	 */
	public static function deleteByID($id) {
		$id = intval($id);
		return EntityManager::entityPassiveDelete(static::getEntityDefinition(), array("id = {$id}"));
	}

	/**
	 * Updates an entity by it's ID.
	 * @param int $id The entity ID.
	 * @param array $properties An associative array containing the properties to be updated.
	 * @return bool
	 */
	public static function updateByID($id, $properties) {
		$id = intval($id);
		return EntityManager::entityPassiveUpdate(static::getEntityDefinition(), array("id = {$id}"), $properties);
	}

	/**
	 * Finds an entity by a property value.
	 * @param string $field The property name.
	 * @param mixed $value The property value.
	 * @return static The entity found, or false if none.
	 */
	public static function findOneBy($field, $value) {
		$results = EntityManager::entityFindByAttributes(static::getEntityDefinition(), array("{$field}" => $value), array('limit' => 1, 'order' => '`id` DESC'));
		if(!$results || sizeof($results) <= 0) {
			return false;
		} else {
			return $results[0];
		}
	}

	/**
	 * Finds all entities that contain a certain property value.
	 * @param string $field The property name.
	 * @param mixed $value The property value
	 * @param array $settings Query select settings, such as order and limit.
	 * @return static[] A list of entities found, or false of none.
	 */
	public static function findAllBy($field, $value, $settings = array()) {
		return EntityManager::entityFindByAttributes(static::getEntityDefinition(), array("{$field}" => $value), $settings);
	}

	/**
	 * Finds all entities that contain a certain set of properties.
	 * @param array $attributes An associative array containing a list of properties and their expected values.
	 * @param array $settings Query select settings, such as order and limit.
	 * @return static[] A list of entities found, or false if none.
	 */
	public static function find($attributes, $settings = array()) {
		return EntityManager::entityFindByAttributes(static::getEntityDefinition(), $attributes, $settings);
	}

	/**
	 * Finds an entity that contain a certain set of properties (the first on the list, if there is more then one).
	 * @param array $attributes An associative array containing a list of properties and their expected values.
	 * @param array $settings Query select settings, such as order and limit.
	 * @return static The entity found, or false if none.
	 */
	public static function findOne($attributes, $settings = array()) {
		$settings['limit'] = 1;
		$results = EntityManager::entityFindByAttributes(static::getEntityDefinition(), $attributes, $settings);
		if(!$results || sizeof($results) <= 0) {
			return false;
		} else {
			return $results[0];
		}
	}

	/**
	 * Finds all entities that fit a certain set of conditions.
	 * @param array $conditionals An array containing a list of conditionals.
	 * @param array $settings Query select settings, such as order and limit.
	 * @return static[] A list of entities found, or false if none.
	 */
	public static function findWhere($conditionals, $settings = array()) {
		if(is_string($conditionals)) {
			$conditionals = array($conditionals);
		}

		return EntityManager::entityFindByConditionals(static::getEntityDefinition(), $conditionals, $settings);
	}

	/**
	 * Gets the amount of entity instances in the database, grouped by each field.
	 * @param string $groupField The grouping field.
	 * @param int $maxResults [optional] The max number of groups to return. Omit or 0 for all.
	 * @return array Associative array containing groups as keys and amounts as values.
	 */
	public static function getGroupCount($groupField, $maxResults = 0) {
		return EntityManager::entityGetGroupCount(static::getEntityDefinition(), $groupField, $maxResults);
	}

	/**
	 * Updates all entities in the database who match a certain set of conditions.
	 * @param array $conditionals An array containing a list of conditionals.
	 * @param array $properties An associative array containing the properties and values that must be updated.
	 * @return bool
	 */
	public static function updateAll($conditionals, $properties) {
		if(is_string($conditionals)) {
			$conditionals = array($conditionals);
		}

		return EntityManager::entityPassiveUpdate(static::getEntityDefinition(), $conditionals, $properties);
	}

	/**
	 * Deletes all entities in the database who match a certain set of conditions.
	 * @param array $conditionals An array containing a list of conditionals.
	 * @return bool
	 */
	public static function deleteAll($conditionals) {
		if(is_string($conditionals)) {
			$conditionals = array($conditionals);
		}

		return EntityManager::entityPassiveDelete(static::getEntityDefinition(), $conditionals);
	}

	/**
	 * Gets a entity query proxy
	 * @param string|null $joinIdentifier The entity identifier for join queries
	 * @return EntityProxy
	 */
	public static function get($joinIdentifier = null) {
		$proxy = new EntityProxy(static::getEntityDefinition(), $joinIdentifier);
		return $proxy->select();
	}

	// ------------------------------------------------------------

	/**
	 * The internal definitions of the classes;
	 * @var EntityDefinition[]
	 */
	protected static $_definitions = null;

	/**
	 * @var string
	 */
	public static $tableName = null;

	/**
	 * Registers an entity class in the ORM.
	 * @param string $tableName The name of the table.
	 * @param array|string $persistentFields [optional] An array of fields to be persistent, or "auto".
	 * 													"auto" will have all fields not starting in "_" to be persistent.
	 */
	public static function registerEntity($tableName, $persistentFields = "auto") {

		$className = get_called_class();

		if($persistentFields == "auto") {

			$persistentFields = array();
			$classProperties = array_keys(get_class_vars($className));

			foreach($classProperties as $property) {
				if($property{0} != "_" && !isset(static::${$property})) {
					array_push($persistentFields, $property);
				}
			}

		}

		static::$_definitions[$className] = new EntityDefinition($className, $tableName, $persistentFields);

	}

	/**
	 * Gets the definition object of this entity.
	 * @return EntityDefinition
	 * @throws \Exception
	 */
	public static function getEntityDefinition() {

		$className = get_called_class();

		if(!isset(static::$_definitions[$className]) || static::$_definitions[$className] == null) {
			if(static::$tableName != null) {
				static::registerEntity(static::$tableName);
			} else {
				throw new \Exception("Entity definition was not registered, and there is no table name defined to register automatically");
			}
		}

		return static::$_definitions[$className];
	}

	// -------------------------------------------------------
	// Implementing ArrayAccess
	// -------------------------------------------------------

	public function offsetExists($offset) {
		return property_exists($this, $offset);
	}

	public function offsetGet($offset) {
		return $this->$offset;
	}

	public function offsetSet($offset, $value) {
		$this->$offset = $value;
	}

	public function offsetUnset($offset) {
		unset($this->$offset);
	}
}

/**
 * Diesel ORM
 * Entity Definition:
 * 		Represents metadata regarding a certain class of entities.
 */
class EntityDefinition {

	public $className;
	public $tableName;
	public $persistentFields;

	public function __construct($className, $tableName, $persistentFields) {
		$this->className = $className;
		$this->tableName = $tableName;
		$this->persistentFields = $persistentFields;
	}

}

/**
 * Diesel ORM - Form element interface
 * 		Provides a common interface from which the ORM's form generators work.
 */
interface IFormElement {

	/**
	 * This function should return a form interface
	 * @return EntityFormInterface
	 */
	public function getFormInterface();

}

/**
 * Diesel ORM - Form interface descriptor:
 *		Describes metadata regarding how a certain entity class has its forms generated.
 */
class EntityFormInterface {

	public $fields;

	/* TODO: implement interface to generate admin */

}

/*******************************************************************************
 * Diesel ORM - Entity Proxy
 *	Acts as a proxy to model instance queries in the database
 ******************************************************************************/
class EntityProxy {

	/**
	 * @var EntityDefinition
	 */
	private $entityDefinition;

	private $queryFields = array();

	private $fromClauses = array();

	private $conditionalClauses = array();
	private $conditionalOperators = array();

	private $orderClauses = array();
	private $groupClauses = array();

	private $isPaged = false;
	private $offset = 0;
	private $limit = 0;


	/**
	 * @var PDOStatement The PDO statement used in the query
	 */
	public $pdoStatement = null;

	private $sqlStatement;

	/**
	 * @param EntityDefinition $entityDefinition The entity definition
	 * @param string $joinIdentifier [optional] The identifier for join operations
	 */
	public function __construct(EntityDefinition $entityDefinition, $joinIdentifier = null) {
		$this->entityDefinition = $entityDefinition;

		if($joinIdentifier != null) {
			$this->fromClauses = array("{$entityDefinition->tableName} {$joinIdentifier}");
		} else {
			$this->fromClauses = array($entityDefinition->tableName);
		}

	}

	/**
	 * Begins a query in the model table
	 *
	 * @return EntityProxy
	 */
	public function select() {
		$this->queryFields = func_get_args();

		if(is_array($this->queryFields[0])) {
			$this->queryFields = $this->queryFields[0];
		}

		return $this;
	}

	/**
	 * Adds a set of conditionals to the WHERE chain
	 * @param array $conditionals List of conditionals
	 * @param string $conditionalGlue Operator to glue the conditionals in this list
	 * @param string $chainGlue Operator to glue this list to the current chain
	 *
	 * @return EntityProxy
	 */
	public function addConditionals($conditionals, $conditionalGlue = "AND", $chainGlue = "AND") {
		if(sizeof($conditionals) <= 0) {
			return $this;
		}

		$conditionals = join(" {$conditionalGlue} ", $conditionals);
		array_push($this->conditionalClauses, $conditionals);
		array_push($this->conditionalOperators, $chainGlue);

		return $this;
	}

	/**
	 * Specifies a list of conditions for the query
	 *
	 * @return EntityProxy
	 */

	public function where() {
		$conditionals = func_get_args();

		if (sizeof($conditionals) == 1 && is_array($conditionals[0])) {
			$conditionals = $conditionals[0];
		}

		return $this->addConditionals($conditionals, "AND", "AND");

	}

	/**
	 * Specifies an alternative (OR) list of conditions for the query
	 *
	 * @return EntityProxy
	 */
	public function also() {
		$conditionals = func_get_args();

		if(sizeof($conditionals) == 1 && is_array($conditionals[0])) {
			$conditionals = $conditionals[0];
		}

		return $this->addConditionals($conditionals, "AND", "OR");
	}

	/**
	 * Specifies a list of possible conditions (glued by OR) for the query
	 *
	 * @return EntityProxy
	 */
	public function either() {
		$conditionals = func_get_args();

		if(sizeof($conditionals) == 1 && is_array($conditionals[0])) {
			$conditionals = $conditionals[0];
		}

		return $this->addConditionals($conditionals, "OR", "AND");
	}

	/**
	 * Specifies an additional list of possible conditions (glued by OR) for the query
	 *
	 * @return EntityProxy
	 */
	public function andEither() {
		$conditionals = func_get_args();

		if(sizeof($conditionals) == 1 && is_array($conditionals[0])) {
			$conditionals = $conditionals[0];
		}

		return $this->addConditionals($conditionals, "OR", "AND");
	}

	/**
	 * Specifies an alternative list of possible conditions (glued by OR) for the query
	 *
	 * @return EntityProxy
	 */
	public function orEither() {
		$conditionals = func_get_args();

		if(sizeof($conditionals) == 1 && is_array($conditionals[0])) {
			$conditionals = $conditionals[0];
		}

		return $this->addConditionals($conditionals, "OR", "OR");
	}

	/**
	 * Specifies a limit to the amount of records returned
	 * Will discard onPage()
	 *
	 * @param int $max The maximum amount of records
	 * @return EntityProxy
	 */
	public function limit($max = 1024) {
		$this->isPaged = true;
		$this->limit = $max;

		return $this;
	}

	/**
	 * Specifies a number of records to offset
	 * Will discard onPage()
	 *
	 * @param $offset int The amount of records to offset
	 * @return EntityProxy
	 */
	public function offset($offset = 0) {
		$this->isPaged = true;
		$this->offset = $offset;

		return $this;
	}

	/**
	 * Pages the query results
	 * Will discard offset() and limit()
	 *
	 * @param int $pageNum The current page number
	 * @param int $perPage The amount of records per page
	 * @return EntityProxy
	 */
	public function onPage($pageNum, $perPage = 24) {
		$this->isPaged = true;
		$this->offset = ($pageNum-1)*$perPage;
		$this->limit = $perPage;

		return $this;
	}

	/**
	 * Specifies an order to the query results
	 *
	 * @return EntityProxy
	 */
	public function orderBy() {

		$ordering = func_get_args();

		if(sizeof($ordering) <= 0) {
			error("No order parameter defined in orderBy() clause");
		}

		if(is_array($ordering[0])) {
			$ordering = $ordering[0];
		}

		if(!$this->orderClauses) {
			$this->orderClauses = $ordering;
		} else {
			array_merge($this->orderClauses, $ordering);
		}

		return $this;

	}

	/**
	 * Specifies a grouping set
	 *
	 * @return EntityProxy
	 */
	public function groupBy() {

		$grouping = func_get_args();

		if(sizeof($grouping) <= 0) {
			error("No grouping parameter defined in groupBy() clause");
		}

		$this->groupClauses = $grouping;

		return $this;

	}

	/**
	 * Performs a natural join with another table for the query results
	 *
	 * @param string $table The table to be joined
	 * @return EntityProxy
	 */
	public function with($table) {
		array_push($this->fromClauses, ", {$table}");
		return $this;
	}

	/**
	 * Performs a left join with another table for the query results
	 *
	 * @param string $table The table to be joined
	 * @param array $on An array of conditions for the join
	 * @return EntityProxy
	 */
	public function leftJoin($table, $on = null) {
		if(is_array($on)) {
			$on = "ON ".join(" AND ", $on);
		} else if(is_string($on)) {
			$on = "ON {$on}";
		}

		array_push($this->fromClauses, "LEFT JOIN {$table} {$on}");
		return $this;
	}

	/**
	 * Performs a right join with another table for the query results
	 *
	 * @param string $table The table to be joined
	 * @param array $on An array of conditions for the join
	 * @return EntityProxy
	 */
	public function rightJoin($table, $on = null) {
		if(is_array($on)) {
			$on = "ON ".join(" AND ", $on);
		} else if(is_string($on)) {
			$on = "ON {$on}";
		}

		array_push($this->fromClauses, "RIGHT JOIN {$table} {$on}");
		return $this;
	}

	/**
	 * Performs an inner join with another table for the query results
	 *
	 * @param string $table The table to be joined
	 * @param array $on An array of conditions for the join
	 * @return EntityProxy
	 */
	public function innerJoin($table, $on = null) {
		if(is_array($on)) {
			$on = "ON ".join(" AND ", $on);
		} else if(is_string($on)) {
			$on = "ON {$on}";
		}

		array_push($this->fromClauses, "INNER JOIN {$table} {$on}");
		return $this;
	}

	/**
	 * Fetches a single model instance from the database
	 *
	 * @return Entity The model instance
	 */
	public function fetchOne() {

		$this->query();

		$obj = $this->pdoStatement->fetchObject($this->entityDefinition->className);

		if($obj === false) {
			return false;
		}

		return $obj;

	}

	private function throwQueryException() {
		$error = EntityManager::getConnection()->errorInfo()[2];

		$msg = "[ORM] PDO query error: [\"{$error}\"]";

		if(config('ERROR_DEBUGGING')) {
			$msg .= "while running query [\"{$this->sqlStatement}\"]";
		}

		throw new EntityManagerException($msg);
	}

	/**
	 * Fetches an array of model instances from the database
	 *
	 * @return Entity[] An array of model instances
	 * @throws PDOException
	 */
	public function fetchAll() {

		if(!$this->query()) {
			$this->throwQueryException();
		}

		$objs = $this->pdoStatement->fetchAll(PDO::FETCH_CLASS, $this->entityDefinition->className);

		if($objs === false) {
			return false;
		}

		return $objs;

	}

	/**
	 * Fetches an array of associative key-values from the database
	 *
	 * @return array
	 * @throws PDOException
	 */
	public function fetchRaw() {

		$this->query();

		if(!$this->query()) {
			$this->throwQueryException();
		}

		return $this->pdoStatement->fetchAll(PDO::FETCH_ASSOC);

	}

	/**
	 * Prepares, executes and returns the prepared PDO statement for the database query
	 *
	 * @return EntityProxy
	 */
	public function query() {

		$this->sqlStatement = "	SELECT
								{$this->prepareFieldClause()}
							FROM
								{$this->prepareFromClause()}
								{$this->prepareWhereClause()}
								{$this->prepareGroupClause()}
								{$this->prepareOrderClause()}
								{$this->prepareLimitClause()}";

		$this->pdoStatement = EntityManager::getConnection()->query($this->sqlStatement);

		return $this->pdoStatement;

	}



	// -------------------------------------------------------------------------

	/**
	 * Prepares the field declaration clause
	 *
	 * @return string Ex.: "*", "id, name", "usr.id, art.title"
	 */
	private function prepareFieldClause() {
		if(sizeof($this->queryFields) > 0) {
			return join(", ", $this->queryFields);
		} else {
			return "*";
		}
	}

	/**
	 * Prepares the FROM clause
	 *
	 * @return string Ex.: "news", "news n, articles a", "news n LEFT JOIN articles a"
	 */
	private function prepareFromClause() {
		return join(" ", $this->fromClauses);
	}

	/**
	 * Prepares the WHERE clause
	 *
	 * @return string Ex.: "WHERE ( x = 1 )", "WHERE ( x = 1 AND x = 2 )", "WHERE (x = 1 AND X = 2) OR ( z = 3 )"
	 */
	private function prepareWhereClause() {
		if(sizeof($this->conditionalClauses) <= 0) {
			return "";
		}

		$conditionals = "WHERE ";

		foreach($this->conditionalClauses as $index => $conditional) {
			$conditionals .= " ({$conditional}) ";
			if($this->conditionalOperators[$index+1]) {
				$conditionals .= $this->conditionalOperators[$index+1];
			}
		}
		return $conditionals;
	}

	/**
	 * Prepares the LIMIT clause
	 *
	 * @return string Ex.: "LIMIT 10", "LIMIT 0,24", "LIMIT 800,24"
	 */
	private function prepareLimitClause() {

		if(!$this->isPaged) {
			return "";
		}

		if($this->offset > 0 || $this->limit > 0) {

			$limit = "LIMIT ";
			if($this->offset > 0) {

				if($this->limit <= 0) {
					$this->limit = 1024;
				}

				$limit .= "{$this->offset},{$this->limit}";
			} else {
				$limit .= "{$this->limit}";
			}

			return $limit;

		} else {
			return "";
		}



	}

	/**
	 * Prepares the ORDER clause
	 *
	 * @return string Ex.: "ORDER BY id DESC", "ORDER BY status ASC, id DESC"
	 */
	private function prepareOrderClause() {

		if(sizeof($this->orderClauses) <= 0) {
			return "";
		}

		return "ORDER BY ".join(", ", $this->orderClauses);
	}

	/**
	 * Prepares the GROUP clause
	 *
	 * @return string Ex.: "GROUP BY user_id", "GROUP BY status, user_id"
	 */
	private function prepareGroupClause() {

		if(sizeof($this->groupClauses) <= 0) {
			return "";
		}

		return "GROUP BY ".join(", ", $this->groupClauses);
	}

	/**
	 * Returns the SQL statement used in the query, if it has already been executed
	 * @return string
	 */
	public function getSQLStatement() {
		return $this->sqlStatement;
	}

}
