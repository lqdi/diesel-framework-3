<?php
class SimpleValidation {

	public static $mode = VALIDATION_MODE_EXCEPTION;
	public static $engine;

	private $lastUsedRule = "unknown";

	/**
	 * Validates a variable against a set of rules
	 * @param string $variable The variable to validate
	 * @param $rules
	 * @return bool|mixed
	 * @throws Exception
	 */
	public function validate($variable, $rules) {
		foreach($rules as $rule) {

			$this->lastUsedRule = $rule;

			$eqPos = strpos($rule, "=") | strpos($rule, ">") | strpos($rule, "<");

			if($eqPos) {
				$ref = trim(substr($rule, $eqPos+1));
				$op = trim(substr($rule, $eqPos, 1));
				$methodParams = array($variable, $op, $ref);
				$rule = trim(substr($rule, 0, $eqPos));
			} else {
				$methodParams = array($variable);
			}

			$methodName = "rule_{$rule}";

			if(!method_exists($this, "rule_{$rule}")) {
				throw new Exception("Validation rule '{$rule}' does not exist!");
			}

			if(!call_user_func_array(array($this, $methodName), $methodParams)) {
				return false;
			}
		}

		return true;
	}

	public function getLastUsedRule() {
		return $this->lastUsedRule;
	}

	// =================================================================================================================

	public function rule_isString($val) {
		return is_string($val);
	}

	public function rule_isInt($val) {
		return filter_var($val, FILTER_VALIDATE_INT);
	}

	public function rule_isFloat($val) {
		return filter_var($val, FILTER_VALIDATE_FLOAT);
	}

	public function rule_isNumeric($val) {
		return is_numeric($val);
	}

	public function rule_isEmail($val) {
		return filter_var($val, FILTER_VALIDATE_EMAIL);
	}

	public function rule_isURL($val) {
		return filter_var($val, FILTER_VALIDATE_URL);
	}

	public function rule_isBoolean($val) {
		return filter_var($val, FILTER_VALIDATE_BOOLEAN);
	}

	public function rule_length($val, $op, $ref) {
		if($op == "=") {
			return strlen($val) == $ref;
		} else if($op == ">") {
			return strlen($val) >= $ref;
		} else {
			return strlen($val) <= $ref;
		}
	}

	public function rule_number($val, $op, $ref) {
		if($op == "=") {
			return intval($val) == $ref;
		} else if($op == ">") {
			return intval($val) >= $ref;
		} else {
			return intval($val) <= $ref;
		}
	}

	public function rule_booleanInt($val) {
		return intval($val) == 0 || intval($val) == 1;
	}

	public function rule_notEmpty($val) {
		return empty($val) && !is_numeric($val);
	}

	public function rule_validDate($val) {
		return (preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $val));
	}

	public function rule_validTime($val) {
		return (preg_match("/^([0-9]{2}):([0-9]{2}):([0-9]{2})$/", $val));
	}

	public function rule_validOrNullTime($val) {
		return (!$val) || $this->rule_validTime($val);
	}

	public function rule_validDateTime($val) {
		return (preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $val));
	}

	public function rule_validMonth($val) {
		return (preg_match("/^([0-9]{4})-([0-9]{2})$/", $val));
	}

	public function rule_dateDiff($val, $op, $ref) {

		if(is_a($val, "DateTime")) {
			$val = $val->getTimestamp();
		} else if(is_string($val)) {
			$val = strtotime($val);
		}

		if(strpos($ref, " ") == -1) {
			$refVal = $ref;
			$refScale = "seconds";
		} else {
			$refVal = substr($ref, 0, strpos($ref, " "));
			$refScale = substr($ref, strpos($ref, " ")+1);
		}

		$refVal = intval($refVal);

		switch($refScale) {
			default: case "seconds": $actualVal = $refVal; break;
			case "minutes": $actualVal = ($refVal * 60); break;
			case "hours": $actualVal = ($refVal) * 3600; break;
			case "days": $actualVal = ($refVal) * 3600 * 24; break;
		}

		$now = time();
		$diff = $now - $actualVal;

		switch($op) {
			default: case "=": return ($diff == $actualVal); break;
			case ">": return ($diff > $actualVal); break;
			case "<": return ($diff < $actualVal); break;
		}

	}

	public function rule_validCPF($val) {
		return Utils::isValidCPF($val);
	}

	public function rule_validCEP($val) {
		return Utils::isValidCEP($val);
	}

	public function rule_validCNPJ($cnpj) {
		$cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);

		if (strlen($cnpj) != 14) return false;

		// Valida primeiro dígito verificador
		for ($i = 0, $j = 5, $sum = 0; $i < 12; $i++) {
			$sum += $cnpj{$i} * $j;
			$j = ($j == 2) ? 9 : $j - 1;
		}

		$remainder = $sum % 11;

		if ($cnpj{12} != ($remainder < 2 ? 0 : 11 - $remainder)) return false;

		for ($i = 0, $j = 6, $sum = 0; $i < 13; $i++) {
			$sum += $cnpj{$i} * $j;
			$j = ($j == 2) ? 9 : $j - 1;
		}

		$remainder = $sum % 11;

		return $cnpj{13} == ($remainder < 2 ? 0 : 11 - $remainder);
	}



	// =================================================================================================================

	/**
	 * Gets the validation engine
	 * @return SimpleValidation
	 */
	public static function getEngine() {
		if(!self::$engine) {
			$engineClass = get_called_class();
			self::$engine = new $engineClass();
		}
		return self::$engine;
	}

}

class SimpleValidationException extends Exception {
	public $fieldName;
	public $fieldValue;
	public $usedRules;
	public $failedRule;

	public function __construct($fieldName, $fieldValue, $usedRules, $failedRule) {
		$this->fieldName = $fieldName;
		$this->fieldValue = $fieldValue;
		$this->usedRules = $usedRules;
		$this->failedRule = $failedRule;
		$this->message = "Invalid field value '{$fieldValue}' for field '{$fieldName}' (failed: {$failedRule})";
	}
}

/**
 * Returns the variable value back if valid, throw an exception if not.
 */
define('VALIDATION_MODE_EXCEPTION', 1);

/**
 * Return true if valid, or false if not.
 */
define('VALIDATION_MODE_BOOLEAN', 2);

/**
 * SimpleValidation: Sets the validation mode
 * 		If the validation mode is Exception, it will return the variable value back if valid, throw an exception if not.
 * 		If the validation mode is Boolean, it will return true if valid, or false if not.
 * @param int $mode VALIDATION_MODE_EXCEPTION or VALIDATION_MODE_BOOLEAN
 */
function setValidationMode($mode) {
	SimpleValidation::$mode = $mode;
}

/**
 * SimpleValidation: Sets the validation engine. Useful if you want to extend SimpleValidation and add new rules
 * @param SimpleValidation $engine
 */
function setValidationEngine(SimpleValidation $engine) {
	SimpleValidation::$engine = $engine;
}

/**
 * SimpleValidation: Validates a variable according to the declared rules
 * @param mixed $variable The variable value
 * @param array $rules An array of rules
 * @param string $fieldName [optional] The name of the field to throw in the exception
 * @return bool|mixed
 * 		If the validation mode is Exception, this will return the variable value back if valid.
 * 		If the validation mode is Boolean, it will return true if valid, or false if not
 * @throws SimpleValidationException If the validation mode is Exception, throws an exception with the informed message when not valid.
 */
function validate($variable, $rules = array(), $fieldName = "unknown") {
	if(SimpleValidation::$mode != VALIDATION_MODE_EXCEPTION) {
		return SimpleValidation::getEngine()->validate($variable, $rules);
	} else {
		if(SimpleValidation::getEngine()->validate($variable, $rules)) {
			return $variable;
		} else {
			throw new SimpleValidationException($fieldName, $variable, $rules, SimpleValidation::getEngine()->getLastUsedRule());
		}
	}
}