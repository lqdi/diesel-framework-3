<?php
header('Content-type: text/css');

require_once './SassParser.php';

function warn($text, $context = null) {
	print "/** WARN: $text";
	if($context) { print ", on line {$context->node->token->line} of {$context->node->token->filename}"; }
	print " **/\n";
}
function debug($text, $context = null) {
	print "/** DEBUG: $text";
	if($context) { print ", on line {$context->node->token->line} of {$context->node->token->filename}"; }
	print " **/\n";
}

function exit_with_visual_print($msg) {

	echo "body::after { display: block; content: '" . addslashes($msg) ."'; position: fixed; top: 10px; left: 10px; background: red; color: white; font-size: 14px; padding: 10px; z-index: 9999999; }";
	exit();

}

// TODO: fix this directly on the nginx redirect directives

// This is a dirty fix for composed SCSS paths
// After we moved from Apache to Nginx, the SCSS compiler redirects started including full unrewritten paths
// So a "css/tokyo.scss" call within "/app/cashflow/invoices/23/" leaves the $_GET['path'] as "/app/cashflow/invoices/23/css/tokyo.scss"
// Here we run through the path, from end to beginning, until we find the root css folder
// This should be able to be fixed directly on Nginx, but I wasn't able to

$pathComponents = explode("/", $_GET['path']);
$pathComponents = array_reverse($pathComponents);

$path = [];

foreach($pathComponents as $component) {
	array_push($path, $component);
	if($component == "css" || $component == "scss") { break; }
}

$path = array_reverse($path); // use push+reverse is more efficient then unshift
$path = join("/", $path);

$file = str_replace("\\","/",__DIR__) ."/../../assets/" . $path . ".scss";

// TODO: implement caching

debug("Compiling file: {$file}");

$syntax = substr($file, -4, 4);

$options = array(
	'style' => 'expanded',
	'cache' => FALSE,
	'syntax' => $syntax,
	'debug' => FALSE,
	'callbacks' => array(
		'warn' => 'warn',
		'debug' => 'debug'
	),
);

// Execute the compiler.
$parser = new SassParser($options);
try {
	print "\n\n" . $parser->toCss($file);
} catch (Exception $e) {
	print $e->getMessage();
}
