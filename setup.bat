@echo off
cls
echo -----------------------------------
echo Diesel Framework 3
echo Copyright © LQDI t.image - 2012
echo Programmed by Aryel Tupinambá
echo -----------------------------------
echo Copying DF3 base files and directories...
xcopy /E /F /Y base_app\* ..\
echo Diesel Framework 3 is ready! Happy coding!
pause